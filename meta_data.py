# -*- coding: utf-8 -*-
"""
Created on Wed May 27 09:07:45 2020

@author: holmanfe
"""

import subprocess
import os
import json
import glob
import pandas as pd
import timeit

class ExifTool(object):

    sentinel = "{ready}\r\n"

    def __init__(self, executable='C:/Users/holmanfe/Documents/exiftool/exiftool(-k).exe'):
        self.executable = executable

    def __enter__(self):
        self.process = subprocess.Popen(
         [self.executable, "-stay_open", "True",  "-@", "-"],
         universal_newlines=True,
         stdin=subprocess.PIPE, stdout=subprocess.PIPE)
        return self

    def  __exit__(self, exc_type, exc_value, traceback):
        self.process.stdin.write("-stay_open\nFalse\n")
        self.process.stdin.flush()

    def execute(self, *args):
        args = args + ("-execute\n",)
        self.process.stdin.write(str.join("\n", args))
        self.process.stdin.flush()
        output = ""
        fd = self.process.stdout.fileno()
        while not output.endswith(self.sentinel):
            output += os.read(fd, 4096).decode('utf-8')
        return output[:-len(self.sentinel)]

    def get_metadata(self, *filenames):
        return json.loads(self.execute("-G", "-j", "-n", *filenames))

def get_meat(image):
    tic=timeit.default_timer()
    with ExifTool() as e:
        meta = e.get_metadata(image)[0]
        # bandName = meta['XMP:BandName']
        # print(bandName)
        return (meta)
    
def df_meat(folder, outfile):
    count=0
    outfile=outfile
    headings=['Image','Band','Date','Aperture','ShutterSpeed','ISO','FocalLength','Lat','Long','Alt','Irradiance','SolarElev','SolarAzimuth','FOV']
    headings_TIR=['Image','Band','Date','Aperture','ShutterSpeed','ISO','FocalLength','Lat','Long','Alt','SensorTemp','FOV']
    tic=timeit.default_timer()
    with ExifTool() as e:
        for im in glob.glob(folder+'*.tif'):
            meta = e.get_metadata(im)[0]
            bandName = meta['XMP:BandName']
            # print(bandName)
            if bandName != 'LWIR':
                df=pd.DataFrame(columns=headings,index=range(0,int(len(glob.glob(folder+'*.tif'))/6)))
                df.loc[df.index[count],headings[0]]=meta['File:FileName']
                df.loc[df.index[count],headings[1]]=meta['XMP:BandName']
                df.loc[df.index[count],headings[2]]=meta['EXIF:CreateDate']
                df.loc[df.index[count],headings[3]]=meta['EXIF:FNumber']
                df.loc[df.index[count],headings[4]]=meta['EXIF:ExposureTime']
                df.loc[df.index[count],headings[5]]=meta['EXIF:ISOSpeed']
                df.loc[df.index[count],headings[6]]=meta['EXIF:FocalLength']
                df.loc[df.index[count],headings[7]]=meta['EXIF:GPSLatitude']
                df.loc[df.index[count],headings[8]]=meta['EXIF:GPSLongitude']
                df.loc[df.index[count],headings[9]]=meta['EXIF:GPSAltitude']
                df.loc[df.index[count],headings[10]]=meta['XMP:Irradiance']
                df.loc[df.index[count],headings[11]]=meta['XMP:SolarElevation']
                df.loc[df.index[count],headings[12]]=meta['XMP:SolarAzimuth']
                df.loc[df.index[count],headings[13]]=meta['Composite:FOV']
                df.to_excel(outfile,sheet_name=bandName)
            
            if bandName == 'LWIR':
                df=pd.DataFrame(columns=headings_TIR,index=range(0,int(len(glob.glob(folder+'*.tif'))/6)))
                df.loc[df.index[count],headings_TIR[0]]=meta['File:FileName']
                df.loc[df.index[count],headings_TIR[1]]=meta['XMP:BandName']
                df.loc[df.index[count],headings_TIR[2]]=meta['EXIF:CreateDate']
                df.loc[df.index[count],headings_TIR[3]]=meta['EXIF:FNumber']
                df.loc[df.index[count],headings_TIR[4]]=meta['EXIF:ExposureTime']
                df.loc[df.index[count],headings_TIR[5]]=meta['EXIF:ISOSpeed']
                df.loc[df.index[count],headings_TIR[6]]=meta['EXIF:FocalLength']
                df.loc[df.index[count],headings_TIR[7]]=meta['EXIF:GPSLatitude']
                df.loc[df.index[count],headings_TIR[8]]=meta['EXIF:GPSLongitude']
                df.loc[df.index[count],headings_TIR[9]]=meta['EXIF:GPSAltitude']
                df.loc[df.index[count],headings_TIR[10]]=meta['XMP:ImagerTemperatureC']
                df.loc[df.index[count],headings_TIR[11]]=meta['Composite:FOV']
                df.to_excel(outfile,sheet_name=bandName)
                count+=1
        toc=timeit.default_timer()
        print(toc - tic)                    
        outfile.save
        return()
    

                
    
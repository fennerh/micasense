# -*- coding: utf-8 -*-
"""
Created on Wed May 27 13:55:47 2020

@author: holmanfe
"""
import os, glob, re
from pyzbar import pyzbar
import cv2
import tifffile as tiff
import numpy as  np
import shutil
from meta_data import *
import fnmatch

def region_stats(img, region, sat_threshold=None):
        """Provide regional statistics for a image over a region
        Inputs: img is any image ndarray, region is a skimage shape
        Outputs: mean, std, count, and saturated count tuple for the region"""
        rev_panel_pts = np.fliplr(region) #skimage and opencv coords are reversed
        w, h, b = img.shape
        # mask = measure.grid_points_in_poly((w,h),rev_panel_pts)
        mask = np.zeros(img.shape[:2], dtype=np.uint8)
        cv2.rectangle(mask, (region[2][0], region[2][1]), (region[0][0], region[0][1]), 255, -1)
        panel_pixels = img[mask==255]
        stdev = panel_pixels.std()
        mean_value = panel_pixels.mean()
        # saturated_count = 0
        # if sat_threshold is not None:
        #     saturated_px = np.asarray(np.where(panel_pixels > sat_threshold))
        #     saturated_count = saturated_px.sum()
        return mean_value, stdev

# def exif_extract(img,count,indexing):
#     headings=['Image','Band','Date','Aperture','ShutterSpeed','ISO','FocalLength','Lat','Long','Alt','Irradiance','SolarElev','SolarAzimuth','FOV']
#     # headings_TIR=['Image','Date','Aperture','ShutterSpeed','ISO','FocalLength','Lat','Long','Alt','SensorTemp','FOV']
#     with ExifTool() as e:
#         meta = e.get_metadata(img)[0]
#         bandName = meta['XMP:BandName']
#         # print(bandName)
    
#         if bandName != 'LWIR':
#             df=pd.DataFrame(columns=headings,index=range(0,int(indexing)))
#             df.loc[df.index[count],headings[0]]=meta['File:FileName']
#             df.loc[df.index[count],headings[1]]=meta['XMP:BandName']
#             df.loc[df.index[count],headings[2]]=meta['EXIF:CreateDate']
#             df.loc[df.index[count],headings[3]]=meta['EXIF:FNumber']
#             df.loc[df.index[count],headings[4]]=meta['EXIF:ExposureTime']
#             df.loc[df.index[count],headings[5]]=meta['EXIF:ISOSpeed']
#             df.loc[df.index[count],headings[6]]=meta['EXIF:FocalLength']
#             df.loc[df.index[count],headings[7]]=meta['EXIF:GPSLatitude']
#             df.loc[df.index[count],headings[8]]=meta['EXIF:GPSLongitude']
#             df.loc[df.index[count],headings[9]]=meta['EXIF:GPSAltitude']
#             df.loc[df.index[count],headings[10]]=meta['XMP:Irradiance']
#             df.loc[df.index[count],headings[11]]=meta['XMP:SolarElevation']
#             df.loc[df.index[count],headings[12]]=meta['XMP:SolarAzimuth']
#             df.loc[df.index[count],headings[13]]=meta['Composite:FOV']
#             # df.to_excel(outfile,sheet_name=bandName)
#             return(df)

# calib_fldr='E:/Calibrations/'
# outfile=outfile= pd.ExcelWriter(os.path.join(calib_fldr,'metadata.xlsx'), engine='xlsxwriter')

# images=[]
# count=0

def calibration_filter(img_flder):
    directory = os.path.dirname(img_flder)
    if not os.path.exists(directory+'/Panel_images/'):
        os.makedirs(directory+'/Panel_images/')
            
    for imge in glob.glob(img_flder+'*_1.tif'):
        img = cv2.imread(imge)
        # meta=get_meat(image)  
         
        ##Using pyzbar locate panel QR code and draw blue rectangle around it.
        try:
            if pyzbar.decode(img)[0].data.decode('utf-8') == 'RP04-1943249-OB_04005391000529':
                shutil.move(imge,directory+'/Panel_images/')
            
                for file in glob.glob(img_flder+'*.tif'):
                    if fnmatch.fnmatch(re.split('(\d+)',file)[-4],re.split('(\d+)',imge)[-4]):
                        shutil.move(file,directory+'/Panel_images/')
                
        except:
            pass
        
        
        
    return(directory+'/Panel_images/')

def panel_boundary(im):
    if re.split('(\d+)',im)[-2] != '6':
        # images.append(im)        
        img = cv2.imread(im)
        meta=get_meat(im)
        
        ##Using pyzbar locate panel QR code and draw blue rectangle around it.
        barcodes = pyzbar.decode(img)
        for barcode in barcodes:
            (x, y, w, h) = barcode.rect
            cv2.rectangle(img, (x, y), (x + w, y + h), (0, 0, 255), 2)
            barcodeData = barcode.data.decode("utf-8")
            barcodeType = barcode.type
            text = "{} ({})".format(barcodeData, barcodeType)
            cv2.putText(img, text, (x, y - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 2)
            # print("[INFO] found {} barcode {}".format(barcodeType, barcodeData))
        
        ##Corner coords of QR code identified from pyzbar##   
        qr_corners=np.asarray([[x,y],[x+w,y],[x+w,y+w],[x,y+w]])
        
        ##Arbitrary coords for ref panel and qr code taken from micasense
        reference_panel_pts = np.asarray([[567, 340], [560, 470], [705, 470], [710, 340]], dtype=np.int32)
        reference_qr_pts = np.asarray([[821, 324], [819, 506], [996, 509], [999, 330]], dtype=np.int32) 
        
        bounds = []
        costs = []
        
        ##calucalte warp based on known and predicted qr location to ID ref panel
        for rotation in range(0,4):
            qr_points = np.roll(reference_qr_pts, rotation, axis=0)
        
            src = np.asarray([tuple(row) for row in qr_points[:3]], np.float32)
            dst = np.asarray([tuple(row) for row in qr_corners[:3]], np.float32)
            warp_matrix = cv2.getAffineTransform(src, dst)
        
            pts = np.asarray([reference_panel_pts], 'int32')
            panel_bounds = cv2.convexHull(cv2.transform(pts, warp_matrix), clockwise=True)
            panel_bounds = np.squeeze(panel_bounds) # remove nested lists
            
            bounds_in_image = True
            for i, pt in enumerate(panel_bounds):
                if pt[0] >= img.shape[0] or pt[0] < 0:
                    bounds_in_image = False
                if pt[1] >= img.shape[1] or pt[1] < 0:
                    bounds_in_image = False
                else:
                    bounds_in_image = True
                    
            # print(bounds_in_image)
            if bounds_in_image:
                mean, std= region_stats(img, panel_bounds, sat_threshold=65000)
                bounds.append(panel_bounds)
                costs.append(std/mean)
        
        idx = costs.index(min(costs))
        panel_bounds = bounds[idx]
        # count=count+1
        return(panel_bounds)
        # if plot == 'True' or plot =='T':
        #     cv2.rectangle(img, (panel_bounds[2][0], panel_bounds[2][1]), (panel_bounds[0][0], panel_bounds[0][1]), (0, 255, 0), 2)       
        #     cv2.imshow(im,cv2.resize(img,(1080,1080)))
        #     cv2.waitKey(0)
        #     cv2.destroyAllWindows()

        

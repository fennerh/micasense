# -*- coding: utf-8 -*-
"""
Created on Thu Jun  4 15:03:28 2020

@author: holmanfe
"""
import os, glob, re, csv
from pyzbar import pyzbar
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import cv2
import fnmatch
import tifffile as tiff
import numpy as  np
from meta_data import *
from panel_QR import *
import utils_AUGSY as utils

img_fldr = 'G:/Flight_2/' 

# headings=['Image','Band','Date','Aperture','ShutterSpeed','ISO','FocalLength','Lat','Long','Alt','Irradiance','SolarElev','SolarAzimuth','FOV']
# headings_TIR=['Image','Band','Date','Aperture','ShutterSpeed','ISO','FocalLength','Lat','Long','Alt','SensorTemp','FOV']

panelCalibration = {
    "Blue": 0.540, 
    "Green": 0.541, 
    "Red": 0.539, 
    "Red edge": 0.538, 
    "NIR": 0.535 
    }

# filter out clibration panel images from image set and move to seperate folder
calib_files=calibration_filter(img_fldr)

headings=['Image','Band','Date','Aperture','ShutterSpeed','ISO','FocalLength','Lat','Long','Alt','Irradiance','SolarElev','SolarAzimuth','FOV',"Panel_rad",'Panel_refl']

with ExifTool() as e:
    with open(calib_files+'panelCalibrations.csv',mode='w') as file:
        writer=csv.DictWriter(file,delimiter=',',fieldnames=headings,lineterminator='\n')
        writer.writeheader()
        for im in glob.glob(calib_files+'*.tif'):
            if re.split('(\d+)',im)[-2] != '6':
                bounds=panel_boundary(im)
                meta = e.get_metadata(im)[0]
                bandName = meta['XMP:BandName']
                imageRaw=plt.imread(im)
                radianceImage, L, V, R = utils.raw_image_to_radiance(meta, imageRaw)
                panel_sub=radianceImage[bounds[0][1]:bounds[2][1], bounds[0][0]:bounds[2][0]]
                panel_rflctnc=radianceImage*(panelCalibration[bandName]/panel_sub.mean())
                writer.writerow({
                    'Image':meta['File:FileName'],
                    'Band':meta['XMP:BandName'],
                    'Date':meta['EXIF:CreateDate'],
                    'Aperture':meta['EXIF:FNumber'],
                    'ShutterSpeed':meta['EXIF:ExposureTime'],
                    'ISO':meta['EXIF:ISOSpeed'],
                    'FocalLength':meta['EXIF:FocalLength'],
                    'Lat':meta['EXIF:GPSLatitude'],
                    'Long':meta['EXIF:GPSLongitude'],
                    'Alt':meta['EXIF:GPSAltitude'],
                    'Irradiance':meta['XMP:Irradiance'],
                    'SolarElev':meta['XMP:SolarElevation'],
                    'SolarAzimuth':meta['XMP:SolarAzimuth'],
                    'FOV':meta['Composite:FOV'],
                    "Panel_rads":panel_sub.mean(),
                    'Panel_refl':panel_rflctnc[bounds[0][1]:bounds[2][1], bounds[0][0]:bounds[2][0]].mean()
                    })
            

        
        

# extract panel metadata including relfectance factors for image calib
count=0
outfile= pd.ExcelWriter(os.path.join(calib_files,'metadata.xlsx'), engine='xlsxwriter')
tic=timeit.default_timer()
with ExifTool() as e:
    for im in glob.glob(calib_files+'*.tif'):
        meta = e.get_metadata(im)[0]
        bandName = meta['XMP:BandName']
        # print(bandName)
        if bandName != 'LWIR':
            df=pd.DataFrame(columns=headings,index=range(0,int(len(glob.glob(calib_files+'*.tif'))/6)))
            df.loc[df.index[count],headings[0]]=meta['File:FileName']
            df.loc[df.index[count],headings[1]]=meta['XMP:BandName']
            df.loc[df.index[count],headings[2]]=meta['EXIF:CreateDate']
            df.loc[df.index[count],headings[3]]=meta['EXIF:FNumber']
            df.loc[df.index[count],headings[4]]=meta['EXIF:ExposureTime']
            df.loc[df.index[count],headings[5]]=meta['EXIF:ISOSpeed']
            df.loc[df.index[count],headings[6]]=meta['EXIF:FocalLength']
            df.loc[df.index[count],headings[7]]=meta['EXIF:GPSLatitude']
            df.loc[df.index[count],headings[8]]=meta['EXIF:GPSLongitude']
            df.loc[df.index[count],headings[9]]=meta['EXIF:GPSAltitude']
            df.loc[df.index[count],headings[10]]=meta['XMP:Irradiance']
            df.loc[df.index[count],headings[11]]=meta['XMP:SolarElevation']
            df.loc[df.index[count],headings[12]]=meta['XMP:SolarAzimuth']
            df.loc[df.index[count],headings[13]]=meta['Composite:FOV']
            df.to_excel(outfile,sheet_name=bandName)
        
        if bandName == 'LWIR':
            df=pd.DataFrame(columns=headings_TIR,index=range(0,int(len(glob.glob(calib_files+'*.tif'))/6)))
            df.loc[df.index[count],headings_TIR[0]]=meta['File:FileName']
            df.loc[df.index[count],headings_TIR[1]]=meta['XMP:BandName']
            df.loc[df.index[count],headings_TIR[2]]=meta['EXIF:CreateDate']
            df.loc[df.index[count],headings_TIR[3]]=meta['EXIF:FNumber']
            df.loc[df.index[count],headings_TIR[4]]=meta['EXIF:ExposureTime']
            df.loc[df.index[count],headings_TIR[5]]=meta['EXIF:ISOSpeed']
            df.loc[df.index[count],headings_TIR[6]]=meta['EXIF:FocalLength']
            df.loc[df.index[count],headings_TIR[7]]=meta['EXIF:GPSLatitude']
            df.loc[df.index[count],headings_TIR[8]]=meta['EXIF:GPSLongitude']
            df.loc[df.index[count],headings_TIR[9]]=meta['EXIF:GPSAltitude']
            df.loc[df.index[count],headings_TIR[10]]=meta['XMP:ImagerTemperatureC']
            df.loc[df.index[count],headings_TIR[11]]=meta['Composite:FOV']
            df.to_excel(outfile,sheet_name=bandName)
            count+=1
toc=timeit.default_timer()
print(toc - tic)                    
    
imageRaw=plt.imread(im)
with ExifTool() as e:
    meta = e.get_metadata(im)[0]
    
radiance,L,V,R = utils.raw_image_to_radiance(meta,imageRaw)

bounds=panel_boundary(im,False)





# for im in glob.glob(img_fldr+'*.tif'):
#     calib_files=calibration_filter(im)
    
for im in glob.glob(calib_files+'*1.tif'):
    print(im)
    for file in glob.glob(img_fldr+'*.tif'):
        if fnmatch.fnmatch(re.split('(\d+)',file)[-4],re.split('(\d+)',im)[-4]):
            shutil.move(file,calib_files)
      

    
# extract metadata for calibration panels, including radiance correction values.
df_meat(calib_files,(calib_files+'metadata.xlsx'))





count=0
outfile= pd.ExcelWriter(os.path.join(calib_files,'metadata.xlsx'), engine='xlsxwriter')
tic=timeit.default_timer()
with ExifTool() as e:
    for im in glob.glob(calib_files+'*.tif'):
        meta = e.get_metadata(im)[0]
        bandName = meta['XMP:BandName']
        # print(bandName)
        if bandName != 'LWIR':
            df=pd.DataFrame(columns=headings,index=range(0,int(len(glob.glob(calib_files+'*.tif'))/6)))
            df.loc[df.index[count],headings[0]]=meta['File:FileName']
            df.loc[df.index[count],headings[1]]=meta['XMP:BandName']
            df.loc[df.index[count],headings[2]]=meta['EXIF:CreateDate']
            df.loc[df.index[count],headings[3]]=meta['EXIF:FNumber']
            df.loc[df.index[count],headings[4]]=meta['EXIF:ExposureTime']
            df.loc[df.index[count],headings[5]]=meta['EXIF:ISOSpeed']
            df.loc[df.index[count],headings[6]]=meta['EXIF:FocalLength']
            df.loc[df.index[count],headings[7]]=meta['EXIF:GPSLatitude']
            df.loc[df.index[count],headings[8]]=meta['EXIF:GPSLongitude']
            df.loc[df.index[count],headings[9]]=meta['EXIF:GPSAltitude']
            df.loc[df.index[count],headings[10]]=meta['XMP:Irradiance']
            df.loc[df.index[count],headings[11]]=meta['XMP:SolarElevation']
            df.loc[df.index[count],headings[12]]=meta['XMP:SolarAzimuth']
            df.loc[df.index[count],headings[13]]=meta['Composite:FOV']
            df.to_excel(outfile,sheet_name=bandName)
        
        if bandName == 'LWIR':
            df=pd.DataFrame(columns=headings_TIR,index=range(0,int(len(glob.glob(calib_files+'*.tif'))/6)))
            df.loc[df.index[count],headings_TIR[0]]=meta['File:FileName']
            df.loc[df.index[count],headings_TIR[1]]=meta['XMP:BandName']
            df.loc[df.index[count],headings_TIR[2]]=meta['EXIF:CreateDate']
            df.loc[df.index[count],headings_TIR[3]]=meta['EXIF:FNumber']
            df.loc[df.index[count],headings_TIR[4]]=meta['EXIF:ExposureTime']
            df.loc[df.index[count],headings_TIR[5]]=meta['EXIF:ISOSpeed']
            df.loc[df.index[count],headings_TIR[6]]=meta['EXIF:FocalLength']
            df.loc[df.index[count],headings_TIR[7]]=meta['EXIF:GPSLatitude']
            df.loc[df.index[count],headings_TIR[8]]=meta['EXIF:GPSLongitude']
            df.loc[df.index[count],headings_TIR[9]]=meta['EXIF:GPSAltitude']
            df.loc[df.index[count],headings_TIR[10]]=meta['XMP:ImagerTemperatureC']
            df.loc[df.index[count],headings_TIR[11]]=meta['Composite:FOV']
            df.to_excel(outfile,sheet_name=bandName)
            count+=1
toc=timeit.default_timer()
print(toc - tic)                    









for im in glob.glob(calib_files+'*.tif'):
    bandname=get_meat(im)
    df,band=df_meat(im,count)
    print(band)
    df_meat(im,count).to_excel(outfile,sheet_name=)
    


        
 
outfile= pd.ExcelWriter(os.path.join(img_fldr,'metadata.xlsx'), engine='xlsxwriter')
headings=['Image','Date','Aperture','ShutterSpeed','ISO','FocalLength','Lat','Long','Alt','Irradiance','SolarElev','SolarAzimuth','FOV']
headings_TIR=['Image','Date','Aperture','ShutterSpeed','ISO','FocalLength','Lat','Long','Alt','SensorTemp','FOV']
count=0

tic=timeit.default_timer()
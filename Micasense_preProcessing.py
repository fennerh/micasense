# -*- coding: utf-8 -*-
"""
Created on Fri Jul 10 13:39:22 2020

@author: holmanfe
"""

from ipywidgets import FloatProgress, Layout
from IPython.display import display
import micasense.imageset as imageset
import micasense.capture as capture
import os, glob
import multiprocessing

panelNames = None
useDLS = True

imagePath = 'C:/Users/holmanfe/Documents/MonsoonProject/Drones/Trinity F90+/Flight_Data/07_07_2020/Altum/images/85/'
panelNames = glob.glob('C:/Users/holmanfe/Documents/MonsoonProject/Drones/Trinity F90+/Flight_Data/07_07_2020/Altum/Calibration/'+'IMG_0001_*.tif')
#panelCap = capture.Capture.from_filelist(panelNames)

outputPath = 'C:/Users/holmanfe/Documents/MonsoonProject/Drones/Trinity F90+/Flight_Data/07_07_2020/Altum/images/85_preprocessed/'
thumbnailPath = os.path.join(outputPath, '..', 'thumbnails')

overwrite = False # can be set to set to False to continue interrupted processing
generateThumbnails = True

if panelNames is not None:
    panelCap = capture.Capture.from_filelist(panelNames)
else:
    panelCap = None

if panelCap is not None:
    if panelCap.panel_albedo() is not None and not any(v is None for v in panelCap.panel_albedo()):
        panel_reflectance_by_band = panelCap.panel_albedo()
    else:
        panel_reflectance_by_band = [0.67, 0.69, 0.68, 0.61, 0.67] #RedEdge band_index order
    
    panel_irradiance = panelCap.panel_irradiance(panel_reflectance_by_band)    
    img_type = "reflectance"
else:
    if useDLS:
        img_type='reflectance'
    else:
        img_type = "radiance"
        
## This progress widget is used for display of the long-running process
f = FloatProgress(min=0, max=1, layout=Layout(width='100%'), description="Loading")
display(f)
def update_f(val):
    if (val - f.value) > 0.005 or val == 1: #reduces cpu usage from updating the progressbar by 10x
        f.value=val

%time imgset = imageset.ImageSet.from_directory(imagePath, progress_callback=update_f)
update_f(1.0)

import math
import numpy as np
from mapboxgl.viz import *
from mapboxgl.utils import df_to_geojson, create_radius_stops, scale_between
from mapboxgl.utils import create_color_stops
import pandas as pd

data, columns = imgset.as_nested_lists()
df = pd.DataFrame.from_records(data, index='timestamp', columns=columns)

#Insert your mapbox token here
token = 'pk.eyJ1IjoibWljYXNlbnNlIiwiYSI6ImNqYWx5dWNteTJ3cWYzMnBicmZid3g2YzcifQ.Zrq9t7GYocBtBzYyT3P4sw'
color_property = 'dls-yaw'
num_color_classes = 8

min_val = df[color_property].min()
max_val = df[color_property].max()

import jenkspy
breaks = jenkspy.jenks_breaks(df[color_property], nb_class=num_color_classes)

color_stops = create_color_stops(breaks,colors='YlOrRd')
geojson_data = df_to_geojson(df,columns[3:],lat='latitude',lon='longitude')

# viz = CircleViz(geojson_data, access_token=token, color_property=color_property,
#                 color_stops=color_stops,
#                 center=[df['longitude'].median(),df['latitude'].median()], 
#                 zoom=16, height='600px',
#                 style='mapbox://styles/mapbox/satellite-streets-v9')
# viz.show()

warp_matrices=None

import exiftool
import datetime
## This progress widget is used for display of the long-running process
f2 = FloatProgress(min=0, max=1, layout=Layout(width='100%'), description="Saving")
display(f2)
def update_f2(val):
    f2.value=val

if not os.path.exists(outputPath):
    os.makedirs(outputPath)
if generateThumbnails and not os.path.exists(thumbnailPath):
    os.makedirs(thumbnailPath)

# Save out geojson data so we can open the image capture locations in our GIS
with open(os.path.join(outputPath,'imageSet.json'),'w') as f:
    f.write(str(geojson_data))
    
try:
    irradiance = panel_irradiance+[0]
except NameError:
    irradiance = None

start = datetime.datetime.now()
for i,capture in enumerate(imgset.captures):
    outputFilename = 'IMG_0'+str(i)+'.tif'
    thumbnailFilename = capture.uuid+'.jpg'
    fullOutputPath = os.path.join(outputPath, outputFilename)
    fullThumbnailPath= os.path.join(thumbnailPath, thumbnailFilename)
    if (not os.path.exists(fullOutputPath)) or overwrite:
        if(len(capture.images) == len(imgset.captures[0].images)):
            capture.create_aligned_capture(irradiance_list=irradiance, warp_matrices=warp_matrices)
            capture.save_capture_as_stack(fullOutputPath)
            if generateThumbnails:
                capture.save_capture_as_rgb(fullThumbnailPath)
    capture.clear_image_data()
    update_f2(float(i)/float(len(imgset.captures)))
update_f2(1.0)
end = datetime.datetime.now()

print("Saving time: {}".format(end-start))
print("Alignment+Saving rate: {:.2f} images per second".format(float(len(imgset.captures))/float((end-start).total_seconds())))

def decdeg2dms(dd):
   is_positive = dd >= 0
   dd = abs(dd)
   minutes,seconds = divmod(dd*3600,60)
   degrees,minutes = divmod(minutes,60)
   degrees = degrees if is_positive else -degrees
   return (degrees,minutes,seconds)

header = "SourceFile,\
GPSDateStamp,GPSTimeStamp,\
GPSLatitude,GpsLatitudeRef,\
GPSLongitude,GPSLongitudeRef,\
GPSAltitude,GPSAltitudeRef,\
FocalLength,\
XResolution,YResolution,ResolutionUnits,\
Image,\
Long,Lat,Alt,\n"

lines = [header]
for capture in imgset.captures:
    #get lat,lon,alt,time
    outputFilename = capture.uuid+'.tif'
    fullOutputPath = os.path.join(outputPath, outputFilename)
    lat,lon,alt = capture.location()
    #write to csv in format:
    # IMG_0199_1.tif,"33 deg 32' 9.73"" N","111 deg 51' 1.41"" W",526 m Above Sea Level
    latdeg, latmin, latsec = decdeg2dms(lat)
    londeg, lonmin, lonsec = decdeg2dms(lon)
    latdir = 'North'
    if latdeg < 0:
        latdeg = -latdeg
        latdir = 'South'
    londir = 'East'
    if londeg < 0:
        londeg = -londeg
        londir = 'West'
    resolution = capture.images[0].focal_plane_resolution_px_per_mm

    linestr = '"{}",'.format(fullOutputPath)
    linestr += capture.utc_time().strftime("%Y:%m:%d,%H:%M:%S,")
    linestr += '"{:d} deg {:d}\' {:.2f}"" {}",{},'.format(int(latdeg),int(latmin),latsec,latdir[0],latdir)
    linestr += '"{:d} deg {:d}\' {:.2f}"" {}",{},{:.1f} m Above Sea Level,Above Sea Level,'.format(int(londeg),int(lonmin),lonsec,londir[0],londir,alt)
    linestr += '{}'.format(capture.images[0].focal_length)
    linestr += '{},{},'.format(resolution,resolution)
    linestr += str(capture.uuid)+'.tif,'
    linestr += str(lon)+','
    linestr += str(lat)+','
    linestr += str(alt)+','
    linestr += '\n' # when writing in text mode, the write command will convert to os.linesep
    lines.append(linestr)

fullCsvPath = os.path.join(outputPath,'log.csv')
with open(fullCsvPath, 'w') as csvfile: #create CSV
    csvfile.writelines(lines)
    
import subprocess

if os.environ.get('exiftoolpath') is not None:
    exiftool_cmd = os.path.normpath(os.environ.get('exiftoolpath'))
else:
    exiftool_cmd = 'exiftool'
        
cmd = '{} -csv="{}" -overwrite_original {}'.format(exiftool_cmd, fullCsvPath, outputPath)
print(cmd)
subprocess.check_call(cmd)